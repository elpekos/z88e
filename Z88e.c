/* Z88E - Z88 Emulator and Debugger */
/* Thierry Peycru, 2021-23          */

#include "z88e.h"

// Z80 CPU instance
Z80 CPU;

// SDL variables
SDL_Window * window;
SDL_Event event;
SDL_TimerID timer_10ms;
SDL_TimerID timer_5ms;

// global variables
uint8_t RunZ88; // 1 when emulating (command / argv)
uint8_t ScreenDouble = 1; // double size (command / argv)
uint32_t ScreenWidth;
uint32_t ScreenFactor;
uint8_t ScreenRedraw = 1; // just redraw LCD off once

const Uint8 * kbst; // SDL keyboard array

//
// Initialize, load OZ and run the main loop
// -----------------------------------------
//
int main(int argc, char** argv)
{
    // memory init
    // load OZ
    FILE *ozfn = fopen("./oz.bin","rb");

    if ( !ozfn ) ExitWithError("OZ file open");
    uint32_t fp = 0;
    while ( !feof(ozfn) )
    {
        ph_mem[fp++] = (uint8_t) fgetc(ozfn);
    }
    fclose(ozfn);

    // memory configuration
    ro_mem[0] = 1; // 512K ROM
    ro_mem[1] = 0; // 512K RAM
    ro_mem[2] = 0; // 512K RAM
    ro_mem[3] = 0; // 512K RAM
    ro_mem[4] = 0; // 512K RAM
    ro_mem[5] = 0; // 512K RAM
    ro_mem[6] = 0; // 512K RAM
    ro_mem[7] = 0; // 512K RAM

    // blink init
    BR_STA     = 0;
    BR_TIM0    = 0;
    BR_TIM1    = 0;
    BR_TIM234  = 0;
    BR_TSTA    = 0;
    BR_TMK     = 0;
    BR_UIT     = 0x11;
    BR_TXD     = 0;

    BB_FLS     = 0; // screen flash switch

    // refresh
    CPU.IPeriod = 1;
    CPU.IAutoReset = 1;
    CPU.IRequest = INT_NONE;

    // shell terminal
    printf("\033[2J"); // cls
    printf("\033[1;1H"); // @1,1
    printf("Z88 emulator and debugger - Thierry Peycru, 2022\n");
    printf("Type H for help.\n>");

    // main loop
    uint8_t dontquit = 1;
    uint8_t mode = 'R';

    while ( dontquit )
    {
        switch(mode)
        {
            case 'Q' : // quit
            {
                dontquit = 0;
                break;    
            }
            case 'R' : // run
            {
                OpenSdl();
                BB_MCK = MCK_ON;
                RunZ88 = 1;
                while (RunZ88)
                {
                    if ( !BB_MCK ) // master clock stopped
                    {
                        SDL_WaitEvent(&event); // idle until event
                        BB_MCK |= DoEvents(&event); // treat event and exit snooze/coma if TIME|KEY
                    }
                    else // master clock is active
                    {
                        while(SDL_PollEvent(&event))
                        {
                            DoEvents(&event); // treat event if any
                        }
                        
                    }
                    for (uint8_t i = 0; i < RATIO_EV_INSTR; i++) // ratio event/instructions
                    {
                    if (BB_INT) // if interrupt
                        {
                            IntZ80(&CPU,INT_IRQ);
                        }
                    ExecZ80(&CPU,1); // execute 1 instruction
                    if (CPU.IFF&IFF_HALT) BB_MCK = MCK_OFF; // if halt switch to coma
                    }
                }
                CloseSdl();
                mode = 'S'; // switch to step debug
            }
            case 'S' : // step debug
            {
                mode = StepZ88();
                break;
            }
        }
    }
    return EXIT_SUCCESS;
}

//
// Manage SDL events
// -----------------
// generate MCK / RTC / keyboard / screen
//
uint8_t DoEvents(SDL_Event * event)
{
    if (event->type == SDL_QUIT)
    {
        RunZ88 = 0; // stop Z88 and exit
        return MCK_NIL;
    }
    if (event->type == SDL_USEREVENT)
    {
        if (event->user.code == 1) // tick : keyboard and RTC
        {
            kbst = SDL_GetKeyboardState(NULL);
            UpdateRTC();
            return MCK_ON; // force master clock
        }
        if (event->user.code == 2) // 100Hz : LCD
        {
            RefreshScreen( window );
            return MCK_NIL;
        }
    }
    if (event->type == SDL_KEYDOWN)
    {
        if ( !BB_MCK ) // clock stopped
        {
        BR_STA |= STA_KEY; // KEY flag
        return MCK_ON; // force master clock
        }
    }
    return MCK_NIL;
}

//
// Open SDL ressources
// -------------------
//
void OpenSdl(void)
{
    // Init screen layout
    ScreenFactor = (ScreenDouble) ? 2 : 1;
    ScreenWidth  = (ScreenDouble) ? SCR_WIDTH*2 : SCR_WIDTH;

    // various SDL init
    if ( SDL_Init(SDL_INIT_VIDEO | SDL_INIT_TIMER) < 0 ) ExitWithError("SDL_Init");
    //atexit( SDL_Quit() );

    window = SDL_CreateWindow("Z88",
        SDL_WINDOWPOS_CENTERED, SDL_WINDOWPOS_CENTERED,
        ScreenWidth, SCR_LINES *8*ScreenFactor,
        SDL_WINDOW_MAXIMIZED | SDL_WINDOW_SHOWN );

    if ( !window ) ExitWithError("SDL_CreateWindow");

    SDL_RaiseWindow(window);

    timer_10ms = SDL_AddTimer(40, TimerCallBack, (void *) 2 );
    if ( !timer_10ms ) ExitWithError("SDL_TimerID 10ms");

    timer_5ms = SDL_AddTimer(5, TimerCallBack, (void *) 1 );
    if ( !timer_5ms ) ExitWithError("SDL_TimerID 5ms");
}

//
// Close SDL ressources
// --------------------
//
void CloseSdl(void)
{
    SDL_RemoveTimer(timer_5ms);
    SDL_RemoveTimer(timer_10ms);
    SDL_DestroyWindow(window);
    SDL_Quit();
    printf("\033[A\33[2K"); // VT100 : up + erase full line
    printf("Stopped\n>");
}

//
// timer call back
// ---------------
// have to use events, call back is another thread
//
uint32_t TimerCallBack(uint32_t period, void *param)
{
    SDL_Event event;
    SDL_UserEvent userevent;
    userevent.type = SDL_USEREVENT;
    userevent.code = (int64_t) param;
    userevent.data1 = NULL;
    userevent.data2 = NULL;
    event.type = SDL_USEREVENT;
    event.user = userevent;
    SDL_PushEvent(&event);
    return(period);
}

//
// update blink RTC
// ----------------
// 
void UpdateRTC(void)
{
    BR_TIM0++;
    if ( BR_TIM0 == 199 )
    {
        BR_TIM0 = 0x00;
        BR_TIM1++;
        if ( BR_TIM1 == 59 )
        {
            BR_TIM1 = 0x00;
            BR_TIM234++;
        }
    } 

    // original Z88 RTC interrupt implementation
    BB_TICK = BR_TIM0 & 0x01; // TSTA bit 0
    BB_SEC  = ( BR_TIM0 == 0x80 ) ? 0x02 : 0x00; // TSTA bit 1
    BB_MIN  = ( BR_TIM1 == 0x20 && BR_TIM0 == 0x00 ) ? 0x04 : 0x00; // TSTA bit 2
    BR_TSTA |= (BB_TICK | BB_SEC | BB_MIN); // TSTA

    // 1 second switch used by screen flash
    if (BB_SEC) BB_FLS = ~BB_FLS;

    // update STA.TIME interrupt bit
    UpdateInt();
}

// refresh screen
// --------------
//
void RefreshScreen(SDL_Window * window)
{
    SDL_Surface * screen = SDL_GetWindowSurface(window);
    if (!screen) ExitWithError("SDL_GetWindowSurface");

    unsigned int * pixels = screen->pixels; // pointer to screen array

    SDL_Color LCD_off  = { 115, 156, 130 };
    SDL_Color LCD_on   = { 17, 18, 64 };
    SDL_Color LCD_grey = { 55,57,140 };

    SDL_LockSurface(screen);
    if (BR_COM & COM_LCDON) // build the screen
    {
        ScreenRedraw = 1; // force refresh for LCD off
        for (uint32_t y = 0; y < SCR_LINES; y++)
        {
            uint32_t xp = 0; // current pixel for pixel array (0-639)
            for (uint32_t x = 0; x < 108; x++)
            {
                uint32_t attra = BR_SBR | y<<8 | x<<1; // attribute address (even)
                uint8_t  attr2 = ph_mem[attra];
                uint8_t  attr1 = ph_mem[attra | 0x00000001];
                uint16_t attri = attr1 << 8 | attr2; // full attribute

                // NULL
                if ( (attr1 & 0x3C) == SBR_NULL ) continue;

                // HIRES
                uint8_t  hrs = ((attr1 & 0x30) == SBR_HRS ) ? 1 : 0;
                uint32_t xr  = (hrs) ? 8 : 6;

                // CURSOR
                uint8_t  crs = ((attr1 & 0x38) == SBR_CRSR) ? 1 : 0;

                // REV
                uint8_t  rev = ((attr1 & 0x30) == SBR_REV ) ? 1 : 0;

                // FLS
                uint8_t  fls = (attr1 & SBR_FLS) ? 1 : 0;

                // GREY
                uint8_t  gry = (attr1 & SBR_GRY) ? 1 : 0;

                // UND
                uint8_t  und = (!hrs) ? (attr1 & SBR_UND) ? 1 : 0 : 0;

                // Select pixel base
                uint32_t pixbase;
                if (hrs) 
                {
                    if ( (attri & SBR_HRS1) == SBR_HRS1 )
                    {
                        pixbase = BR_PB3 | (uint32_t) ((attri & 0x00FF)<<3); // HIRES1 (ROM 256)
                    }
                    else
                    {
                        pixbase = BR_PB2 | (uint32_t) ((attri & 0x03FF)<<3); // HIRES0 (RAM 768)
                    }
                }
                else
                {
                    if ( (attri & SBR_LRS0) == SBR_LRS0 )
                    {
                        pixbase = BR_PB0 | (uint32_t) ((attri & 0x003F)<<3); // LORES0 (RAM 64)
                    }
                    else
                    {
                        pixbase = BR_PB1 | (uint32_t) ((attri & 0x01FF)<<3); // LORES1 (ROM 448)  
                    }
                }

                // Apply attributes
                SDL_Color LCD_1 = (gry) ? LCD_grey : LCD_on;
                SDL_Color LCD_0 = LCD_off;
                SDL_Color LCD_tmp;

                if (crs && BB_FLS)
                {
                    rev = (!rev) ? 1 : 0;
                    fls = 0;
                }

                if (fls && BB_FLS)
                {
                    LCD_1 = LCD_off;
                }

                if (rev)
                {
                    LCD_tmp = LCD_0;
                    LCD_0 = LCD_1;
                    LCD_1 = LCD_tmp;
                }

                for (uint32_t yb = 0; yb < 8; yb++) // 8 lines per char
                {
                    uint8_t pxcline = ph_mem[pixbase | yb]; // the 8 pixels

                    if ((yb == 7) && und) pxcline = 0xFF;
                    SDL_Color LCD_pix[8];
                    if (hrs)
                    {
                        LCD_pix[0] = ( pxcline & 0x80 ) ? LCD_1 : LCD_0;
                        LCD_pix[1] = ( pxcline & 0x40 ) ? LCD_1 : LCD_0;
                        LCD_pix[2] = ( pxcline & 0x20 ) ? LCD_1 : LCD_0;
                        LCD_pix[3] = ( pxcline & 0x10 ) ? LCD_1 : LCD_0;
                        LCD_pix[4] = ( pxcline & 0x08 ) ? LCD_1 : LCD_0;
                        LCD_pix[5] = ( pxcline & 0x04 ) ? LCD_1 : LCD_0;
                        LCD_pix[6] = ( pxcline & 0x02 ) ? LCD_1 : LCD_0;
                        LCD_pix[7] = ( pxcline & 0x01 ) ? LCD_1 : LCD_0;                        
                    }
                    else
                    {
                        LCD_pix[0] = ( pxcline & 0x20 ) ? LCD_1 : LCD_0;
                        LCD_pix[1] = ( pxcline & 0x10 ) ? LCD_1 : LCD_0;
                        LCD_pix[2] = ( pxcline & 0x08 ) ? LCD_1 : LCD_0;
                        LCD_pix[3] = ( pxcline & 0x04 ) ? LCD_1 : LCD_0;
                        LCD_pix[4] = ( pxcline & 0x02 ) ? LCD_1 : LCD_0;
                        LCD_pix[5] = ( pxcline & 0x01 ) ? LCD_1 : LCD_0;
                    }

                    uint32_t xc = (y<<3 | yb)*ScreenWidth + xp; // current position in screen
                    for (uint32_t xb = 0; xb < xr; xb++)
                    {
                        if ( (xb+xp) > 639 ) break;
                        uint32_t pixel = xc + xb;
                        if (ScreenDouble)
                        {
                            pixels[pixel*2                  ] = SDL_MapRGB(screen->format, LCD_pix[xb].r, LCD_pix[xb].g, LCD_pix[xb].b );
                            pixels[pixel*2 +1               ] = SDL_MapRGB(screen->format, LCD_pix[xb].r, LCD_pix[xb].g, LCD_pix[xb].b );
                            pixels[pixel*2 + ScreenWidth    ] = SDL_MapRGB(screen->format, LCD_pix[xb].r, LCD_pix[xb].g, LCD_pix[xb].b );
                            pixels[pixel*2 + ScreenWidth +1 ] = SDL_MapRGB(screen->format, LCD_pix[xb].r, LCD_pix[xb].g, LCD_pix[xb].b );
                        }
                        else
                        {
                            pixels[pixel] = SDL_MapRGB(screen->format, LCD_pix[xb].r, LCD_pix[xb].g, LCD_pix[xb].b );
                        }
                    }
                }
                xp = xp + xr;
            }
        }
    }
    else if ( ScreenRedraw ) // just fill the screen with LCD OFF color
    {
        for (uint16_t yy = 0; yy < SCR_LINES*8*ScreenFactor; yy++)
        {
            for (uint16_t xx = 0; xx < ScreenWidth; xx++)
            {
            pixels[yy * ScreenWidth + xx] = SDL_MapRGB(screen->format, LCD_off.r, LCD_off.g, LCD_off.b );
            }
        }
        ScreenRedraw = 0; // stop refresh
    }
    SDL_UnlockSurface(screen);
    if (SDL_UpdateWindowSurface(window)) ExitWithError("SDL_UpdateWindowSurface");
}

// error manager
// -------------
//
int ExitWithError(char * message)
{
    printf("\n%s failed : %s\n", message, SDL_GetError() );
    exit(EXIT_FAILURE);    
}

/** RdZ80()/WrZ80() ******************************************/
/** These functions are called when access to RAM occurs.   **/
/** They allow to control memory access.                    **/
/************************************ TO BE WRITTEN BY USER **/

/*------------*/
/* memory I/O */
/*------------*/

/* Convert logical address (16bit) to physical address (22bit) */
uint32_t ToPhAddr(register word Addr)
{
    if ( Addr & 0xC000 ) // segment 1, 2, 3
        return ( BR_SR[ Addr >> 14 ] << 14 ) | ( Addr & 0x3FFF );
    else if ( Addr & 0x2000 ) // segment 0 upper half
        return ( (BR_SR[0] & 0xFE) << 14 ) | ((BR_SR[0] & 0x01)<<13) | (Addr & 0x1FFF);
    else if ( BR_COM & COM_RAMS ) // lower half is system RAM
        return ( 0x20 << 14 ) | ( Addr & 0x1FFF );
    else // lower half is system ROM
        return ( Addr & 0x1FFF );
}

/* Test if address is writable */
uint8_t MemIsRom(register uint32_t PhAddr)
{
    return ro_mem[ PhAddr >> 19 ];
}

/* Write byte to logical memory */
void WrZ80(register word Addr,register byte Value)
{
    uint32_t PhAddr = ToPhAddr(Addr);
    if ( !MemIsRom(PhAddr) ) 
    {
        ph_mem[ PhAddr ] = Value;
    }
}

/* Read byte from logical memory */
byte RdZ80(register word Addr)
{
    return ph_mem[ ToPhAddr(Addr) ];
}

/** InZ80()/OutZ80() *****************************************/
/** Z80 emulation calls these functions to read/write from  **/
/** I/O ports. There can be 65536 I/O ports, but only first **/
/** 256 are usually used                                    **/
/************************************ TO BE WRITTEN BY USER **/

/*-----------*/
/* blink I/O */
/*-----------*/

/* Register Write */
void OutZ80(register word Port,register byte Value)
{
    switch ( Port & 0x00FF )
    {
        case BL_PB0  : BR_PB0   = ( Value | ( Port & 0x1F00 ) ) <<  9;return; // LR0 13 bits {5+8} (512 bytes)
        case BL_PB1  : BR_PB1   = ( Value | ( Port & 0x0300 ) ) << 12;return; // LR1 10 bits {2+8} (4K)
        case BL_PB2  : BR_PB2   = ( Value | ( Port & 0x0100 ) ) << 13;return; // HR0  9 bits {1+8} (8K)
        case BL_PB3  : BR_PB3   = ( Value | ( Port & 0x0700 ) ) << 11;return; // HR1 11 bits {3+8} (2K)
        case BL_SBR  : BR_SBR   = ( Value | ( Port & 0x0700 ) ) << 11;return; // SBF 11 bits {3+8} (2K)
        case BL_COM  :
        {
            if ( Value & COM_RESTIM )
            {
                BR_TIM0   = 0;
                BR_TIM1   = 0;
                BR_TIM234 = 0;  
            }
            BR_COM  = Value & ( ~COM_RESTIM );
            return;   
        }
        case BL_INT  :
        {
            BR_INT   = Value & ( INT_GINT | INT_TIME | INT_KEY | INT_KWAIT );
            UpdateInt();
            return;
        }
        case BL_EPR  : BR_EPR   = Value;return;
        case BL_TACK : 
        {
            if ( Value & TACK_TICK ) BR_TSTA &= ~TSTA_TICK; // ack tick int
            if ( Value & TACK_SEC )  BR_TSTA &= ~TSTA_SEC;  // ack sec int
            if ( Value & TACK_MIN )  BR_TSTA &= ~TSTA_MIN;  // ack min int
            UpdateInt();
            return;
        }
        case BL_TMK  :
        {
            BR_TMK   = Value & ( TMK_TICK | TMK_SEC | TMK_MIN );
            UpdateInt();
            return;            
        }
        case BL_ACK  :
        {
            if ( Value & ACK_KEY ) BR_STA &= ~STA_KEY; // add BTL/FLAP/UART when implemented
            UpdateInt();
            return;
        }
        case BL_SR0  : BR_SR[0] = Value;return;
        case BL_SR1  : BR_SR[1] = Value;return;
        case BL_SR2  : BR_SR[2] = Value;return;
        case BL_SR3  : BR_SR[3] = Value;return;
        case BL_RXC  : BR_RXC   = Value;return;
        case BL_TXD  :
        {
            BR_TXD = Value;
            fprintf(stdout,"%c", BR_TXD);
            fflush(stdout);
            return;
        }
        case BL_TXC  : BR_TXC   = Value;return;
        case BL_UMK  : BR_UMK   = Value;return;
        case BL_UAK  : BR_UAK   = Value;return;
    }
}

/* Update interrupt */
void UpdateInt(void)
{
    // TIME interrupt
    if ( BR_TSTA & BR_TMK )  BR_STA |=  STA_TIME;
    else                     BR_STA &= ~STA_TIME;
    BB_TIME = ( BR_STA & STA_TIME ) ? INT_TIME : 0x00; // TIME interrupt without INT mask, not the same bit (1) as STA (0)

    // INT
    BB_GINT = ( BR_INT & INT_GINT ) ? INT_TIME | INT_KEY : 0x00; // INT enable mask, add BTL/FLAP/UART when implemented
    BB_INT  = BB_GINT & BR_INT & (BB_TIME | (BR_STA & STA_KEY)); // interrupt are driven by TIME and KEY
}

/* Register Read */
byte InZ80(register word Port)
{
    switch ( Port & 0x00FF )
    {
        case BL_STA : return BR_STA;
        case BL_KBD :
        {
            if ( BL_INT & INT_KWAIT ) // snooze
            {
                BR_INT &= ~INT_KWAIT; // toggle KWAIT
                BB_MCK = MCK_OFF; // stop main clock until interrupt
            }
            uint8_t km = KeyboardMatrix( (uint8_t) ((Port & 0xFF00) >> 8 ));
            return km;
        }
        case BL_TSTA : return BR_TSTA;
        case BL_TIM0 : return BR_TIM0;
        case BL_TIM1 : return BR_TIM1;
        case BL_TIM2 : return (byte) (BR_TIM234 & 0x000000FF);
        case BL_TIM3 : return (byte) (BR_TIM234 & 0x0000FF00) >> 8;
        case BL_TIM4 : return (byte) (BR_TIM234 & 0x001F0000) >> 16; // 5 bits
        case BL_RXD  : return BR_RXD;
        case BL_RXE  : return BR_RXE;
        case BL_UIT  : return BR_UIT;
        default      : return 0x00;
    }
}

/** LoopZ80() ************************************************/
/** Z80 emulation calls this function periodically to check **/
/** if the system hardware requires any interrupts. This    **/
/** function must return an address of the interrupt vector **/
/** (0x0038, 0x0066, etc.) or INT_NONE for no interrupt.    **/
/** Return INT_QUIT to exit the emulation loop.             **/
/************************************ TO BE WRITTEN BY USER **/
word LoopZ80(register Z80 *R)
{
    //if (BR_STA & STA_TIME) return INT_IRQ;
    return INT_NONE;
}

/** PatchZ80() ***********************************************/
/** Z80 emulation calls this function when it encounters a  **/
/** special patch command (ED FE) provided for user needs.  **/
/** For example, it can be called to emulate BIOS calls,    **/
/** such as disk and tape access. Replace it with an empty  **/
/** macro for no patching.                                  **/
/************************************ TO BE WRITTEN BY USER **/
void PatchZ80(register Z80 *R)
{}

//
// Help
// ----
//
void DebugHelp(void)
{
    printf("R run emulator\n");
    printf("Q exit\n");
    printf("S view blink registers\n");
    printf("ENTER execute one instruction\n");
    printf("\n>");
}

//
// Debug command line 
// ------------------
//
char StepZ88(void)
{
    char st[128];
    uint8_t i;
    while(1)
    {
        fflush(stdout);fflush(stdin);
        fgets(st,50,stdin);
        for(i=0;st[i]>=' ';i++)
        {
            st[i]=tolower(st[i]);    
        }
        st[i]='\0';
        switch(st[0])
        {
            case 'q' : return 'Q';
            case 'r' : return 'R';
            case 's' : ViewBlink();break;
            case 'h' : DebugHelp();break;
            case '\0': OneStep();
        }
    }
}

//
// Instruction debug line
// ----------------------
//
void OneStep(void)
{
    char S[128];
    uint16_t N = DAsm(S,CPU.PC.W);
    printf("\033[A\33[2K"); // VT100 : up + erase full line
    printf("%02X-%04X ", (uint8_t) (ToPhAddr(CPU.PC.W)>>14), CPU.PC.W);
    for (uint16_t I=0; I<4; I++)
    {
        if (N)
        {
            printf("%02X ",ph_mem[ToPhAddr(CPU.PC.W+I)]);
            N--;
        }
        else printf("   ");
    }
    printf("%-20s ", S);
    printf("%02X %c%c%c%c%c%c%c%c ", CPU.AF.B.h, FLAGS(CPU.AF.B.l));
    printf("%04X %04X %04X  ", CPU.BC.W, CPU.DE.W, CPU.HL.W);
    printf("%02X %c%c%c%c%c%c%c%c ", CPU.AF1.B.h, FLAGS(CPU.AF1.B.l));
    printf("%04X %04X %04X  ", CPU.BC1.W, CPU.DE1.W, CPU.HL1.W);
    printf("%04X %04X  (%04X) = %02X%02X", CPU.IX.W, CPU.IY.W, CPU.SP.W, ph_mem[ToPhAddr(CPU.SP.W+1)], ph_mem[ToPhAddr(CPU.SP.W)]);
    printf("\n>");
    ExecZ80(&CPU,1);
}

//
// Show blink registers 
// --------------------
//
void ViewBlink(void)
{
    printf("LOWR:%s ", (BR_COM & COM_RAMS) ? "rams" : "roms" );
    printf(" SR0:%02X   ", BR_SR[0]);
    printf(" SR1:%02X   ", BR_SR[1]);
    printf(" SR2:%02X   ", BR_SR[2]);
    printf(" SR3:%02X\n" , BR_SR[3]);
    printf(" SBR:%04X "  , BR_SBR >>11);
    printf(" PB0:%04X "  , BR_PB0 >>9 );
    printf(" PB1:%04X "  , BR_PB1 >>12);
    printf(" PB2:%04X "  , BR_PB2 >>13);
    printf(" PB3:%04X\n" , BR_PB3 >>11);
    printf("TIM0:%02X   ", BR_TIM0);
    printf("TIM1:%02X   ", BR_TIM1);
    printf("TIM2:%02X   ", (BR_TIM234 & 0x000000FF));
    printf("TIM3:%02X   ", (BR_TIM234 & 0x0000FF00) >> 8);
    printf("TIM4:%02X\n" , (BR_TIM234 & 0x001F0000) >> 16);
    printf(" STA:%02X   ", BR_STA);
    printf("TSTA:%02X\n" , BR_TSTA);
    printf("\n>");
}

//
// Return BL_KBD
// -------------
//
uint8_t KeyboardMatrix( uint8_t Mask )
{
    uint8_t Row[8], krm[8];

    krm[7] = (0x80 & ~Mask) ? 0xFF : 0x00;
    krm[6] = (0x40 & ~Mask) ? 0xFF : 0x00;
    krm[5] = (0x20 & ~Mask) ? 0xFF : 0x00;
    krm[4] = (0x10 & ~Mask) ? 0xFF : 0x00;
    krm[3] = (0x08 & ~Mask) ? 0xFF : 0x00;
    krm[2] = (0x04 & ~Mask) ? 0xFF : 0x00;
    krm[1] = (0x02 & ~Mask) ? 0xFF : 0x00;
    krm[0] = (0x01 & ~Mask) ? 0xFF : 0x00;

    Row[0] = ( (kbst[SDL_SCANCODE_RSHIFT]        & krm[7]) | 
               (kbst[SDL_SCANCODE_F1]            & krm[6]) | // Help
               (kbst[SDL_SCANCODE_LEFTBRACKET]   & krm[5]) | 
               (kbst[SDL_SCANCODE_RIGHTBRACKET]  & krm[4]) | 
               (kbst[SDL_SCANCODE_EQUALS]        & krm[3]) | 
               (kbst[SDL_SCANCODE_MINUS]         & krm[2]) | 
               (kbst[SDL_SCANCODE_BACKSLASH]     & krm[1]) | 
               (kbst[SDL_SCANCODE_BACKSPACE]     & krm[0]) ) ? 0x80 : 0x00;

    Row[1] = ( (kbst[SDL_SCANCODE_LALT]          & krm[7]) | // []
               (kbst[SDL_SCANCODE_LSHIFT]        & krm[6]) |
               (kbst[SDL_SCANCODE_SPACE]         & krm[5]) |
               (kbst[SDL_SCANCODE_LEFT]          & krm[4]) |
               (kbst[SDL_SCANCODE_RIGHT]         & krm[3]) |
               (kbst[SDL_SCANCODE_DOWN]          & krm[2]) |
               (kbst[SDL_SCANCODE_UP]            & krm[1]) |
               (kbst[SDL_SCANCODE_RETURN]        & krm[0]) ) ? 0x40 : 0x00;
    
    Row[2] = ( (kbst[SDL_SCANCODE_ESCAPE]        & krm[7]) |
               (kbst[SDL_SCANCODE_TAB]           & krm[6]) |
               (kbst[SDL_SCANCODE_1]             & krm[5]) |
               (kbst[SDL_SCANCODE_2]             & krm[4]) |
               (kbst[SDL_SCANCODE_3]             & krm[3]) |
               (kbst[SDL_SCANCODE_4]             & krm[2]) |
               (kbst[SDL_SCANCODE_5]             & krm[1]) |
               (kbst[SDL_SCANCODE_6]             & krm[0]) ) ? 0x20 : 0x00;

    Row[3] = ( (kbst[SDL_SCANCODE_F2]            & krm[7]) | // Index
               (kbst[SDL_SCANCODE_LCTRL]         & krm[6]) | // <>
               (kbst[SDL_SCANCODE_Q]             & krm[5]) |
               (kbst[SDL_SCANCODE_W]             & krm[4]) |
               (kbst[SDL_SCANCODE_E]             & krm[3]) |
               (kbst[SDL_SCANCODE_R]             & krm[2]) |
               (kbst[SDL_SCANCODE_T]             & krm[1]) |
               (kbst[SDL_SCANCODE_Y]             & krm[0]) ) ? 0x10 : 0x00;

    Row[4] = ( (kbst[SDL_SCANCODE_RALT]          & krm[7]) | // CAPS (SDL_SCANCODE_CAPSLOCK is buggy)
               (kbst[SDL_SCANCODE_F3]            & krm[6]) | // Menu
               (kbst[SDL_SCANCODE_A]             & krm[5]) |
               (kbst[SDL_SCANCODE_S]             & krm[4]) |
               (kbst[SDL_SCANCODE_D]             & krm[3]) |
               (kbst[SDL_SCANCODE_F]             & krm[2]) |
               (kbst[SDL_SCANCODE_G]             & krm[1]) |
               (kbst[SDL_SCANCODE_H]             & krm[0]) ) ? 0x08 : 0x00;

    Row[5] = ( (kbst[SDL_SCANCODE_PERIOD]        & krm[7]) |
               (kbst[SDL_SCANCODE_COMMA]         & krm[6]) |
               (kbst[SDL_SCANCODE_Z]             & krm[5]) |
               (kbst[SDL_SCANCODE_X]             & krm[4]) |
               (kbst[SDL_SCANCODE_C]             & krm[3]) |
               (kbst[SDL_SCANCODE_V]             & krm[2]) |
               (kbst[SDL_SCANCODE_B]             & krm[1]) |
               (kbst[SDL_SCANCODE_N]             & krm[0]) ) ? 0x04 : 0x00;

    Row[6] = ( (kbst[SDL_SCANCODE_SLASH]         & krm[7]) |
               (kbst[SDL_SCANCODE_SEMICOLON]     & krm[6]) |
               (kbst[SDL_SCANCODE_L]             & krm[5]) |
               (kbst[SDL_SCANCODE_M]             & krm[4]) |
               (kbst[SDL_SCANCODE_K]             & krm[3]) |
               (kbst[SDL_SCANCODE_J]             & krm[2]) |
               (kbst[SDL_SCANCODE_U]             & krm[1]) |
               (kbst[SDL_SCANCODE_7]             & krm[0]) ) ? 0x02 : 0x00;

    Row[7] = ( (kbst[SDL_SCANCODE_APOSTROPHE]    & krm[7]) | // £ Pound
               (kbst[SDL_SCANCODE_NONUSBACKSLASH]& krm[6]) | // ` " key
               (kbst[SDL_SCANCODE_0]             & krm[5]) |
               (kbst[SDL_SCANCODE_P]             & krm[4]) |
               (kbst[SDL_SCANCODE_9]             & krm[3]) |
               (kbst[SDL_SCANCODE_O]             & krm[2]) |
               (kbst[SDL_SCANCODE_I]             & krm[1]) |
               (kbst[SDL_SCANCODE_8]             & krm[0]) ) ? 0x01 : 0x00;

    return   ~(Row[0] | Row[1] | Row[2] | Row[3] | Row[4] | Row[5] | Row[6] | Row[7]);
}
