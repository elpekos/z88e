# Z88E makefile
# Thierry Peycru, 2022

CC=cc
CFLAGS=`sdl2-config --cflags --libs`
LIBSDL=-I/opt/homebrew/include -L/opt/homebrew/lib -lSDL2

# by default, never forget tab before command
z88e: dasm.c Z80.c Z88e.c
	$(CC) -o z88e Z88e.c Z80.c dasm.c $(CFLAGS) $(LIBSDL)
	chmod +x z88e

# debug
# during debugging, add format of vars with
#type format add -f hex uint8_t
#type format add -f hex uint16_t
#type format add -f hex uint32_t
debug:
	$(CC) -g -o z88dbg Z88e.c Z80.c dasm.c $(CFLAGS) $(LIBSDL)
	lldb z88dbg

# cleanup
clean:
	rm -Rf z88e z88dbg z88dbg.dSYM
	clear
