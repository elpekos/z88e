/* ZED - Z88 Emulator and Debugger */
/* Thierry Peycru, 2021-22         */

/* Blink Registers */

#ifndef BLINK_H
#define BLINK_H

/* Register Write */
#define BL_PB0      0x70
#define BL_PB1      0x71
#define BL_PB2      0x72
#define BL_PB3      0x73
#define BL_SBR      0x74
#define BL_COM      0xB0
#define     COM_LCDON   0x01
#define     COM_VPPON   0x02
#define     COM_RAMS    0x04
#define     COM_PROGRAM 0x08
#define     COM_RESTIM  0x10
#define     COM_OVERP   0x20
#define     COM_SBIT    0x40
#define     COM_SRUN    0x80
#define BL_INT      0xB1
#define     INT_GINT    0x01
#define     INT_TIME    0x02
#define     INT_KEY     0x04
#define     INT_BTL     0x08
#define     INT_UART    0x10
#define     INT_FLAP    0x20
#define     INT_A19     0x40
#define     INT_KWAIT   0x80
#define BL_EPR      0xB3
#define BL_TACK     0xB4
#define     TACK_TICK   0x01
#define     TACK_SEC    0x02
#define     TACK_MIN    0x04
#define BL_TMK      0xB5
#define     TMK_TICK    0x01
#define     TMK_SEC     0x02
#define     TMK_MIN     0x04
#define BL_ACK      0xB6
#define     ACK_KEY     0x04
#define     ACK_BTL     0x08
#define     ACK_UART    0x10
#define     ACK_FLAP    0x20
#define     ACK_A19     0x40
#define BL_SR0      0xD0
#define BL_SR1      0xD1
#define BL_SR2      0xD2
#define BL_SR3      0xD3
#define BL_RXC      0xE2  
#define BL_TXD      0xE3
#define BL_TXC      0xE4
#define BL_UMK      0xE5
#define BL_UAK      0xE6

/* Register Read */
#define BL_KBD      0xB2
#define BL_STA      0xB1
#define     STA_TIME    0x01
#define     STA_KEY     0x04
#define     STA_BTL     0x08
#define     STA_UART    0x10
#define     STA_FLAP    0x20
#define     STA_A19     0x40
#define     STA_FLAPOPEN 0x80
#define BL_TSTA     0xB5
#define     TSTA_TICK   0x01
#define     TSTA_SEC    0x02
#define     TSTA_MIN    0x04
#define BL_TIM0     0xD0
#define BL_TIM1     0xD1
#define BL_TIM2     0xD2
#define BL_TIM3     0xD3
#define BL_TIM4     0xD4
#define BL_RXD      0xE0
#define BL_RXE      0xE1
#define BL_UIT      0xE5
#define     UIT_CTS     0x01
#define     UIT_DCD     0x02
#define     UIT_RDRF    0x04
#define     UIT_TDRE    0x10
#define     UIT_CTSI    0x20
#define     UIT_DCDI    0x40
#define     UIT_RSRD    0x80

/* Screen */
#define SCR_LINES   8
#define SCR_WIDTH   640

/* Screen files masks */
#define SBR_NULL    0x34
#define SBR_CRSR    0x38
#define SBR_REV     0x10
#define SBR_UND     0x02
#define SBR_GRY     0x04
#define SBR_FLS     0x08
#define SBR_HRS     0x20
#define SBR_HRS1    0x0300
#define SBR_LRS0    0x01C0

/* Z80 master clock */
#define MCK_NIL     0 // dont change
#define MCK_ON      1 // clock is active
#define MCK_OFF     0 // master clock stopped (snooze/coma)

/* Blink registers */
uint32_t BR_PB0, BR_PB1, BR_PB2, BR_PB3, BR_SBR;
uint8_t BR_TIM0, BR_TIM1;
uint32_t BR_TIM234;
uint8_t BR_COM, BR_INT, BR_EPR, BR_TMK;
uint8_t BR_SR[4];
uint8_t BR_RXC, BR_TXD, BR_TXC, BR_UMK, BR_UAK;

uint8_t BR_KBD, BR_STA, BR_TSTA;
uint8_t BR_RXD, BR_RXE, BR_UIT;

uint8_t BB_TICK, BB_SEC, BB_MIN;
uint8_t BB_GINT, BB_TIME, BB_INT;
uint8_t BB_FLS;

uint8_t BB_MCK; // master clock

/* Physical memory array */
uint8_t ph_mem[1<<22];
uint8_t ro_mem[8]; // 0/1 slot 0, 2/3 slot 1, 4/5 slot 2, 6/7 slot 3 (1 if ROM, 0 if RAM)

#endif
