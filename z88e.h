/* Z8E - Z88 Emulator and Debugger */
/* Thierry Peycru, 2021-23         */

#ifndef Z88E_H
#define Z88E_H

/* all the includes */
#include <stdint.h>
#include <stdlib.h>
#include <stdio.h>
#include <strings.h>
#include <ctype.h>
#include <SDL2/SDL.h>

#include "Z80.h"
#include "blink.h"

/* number of instruction executed before polling an SDL event */
#define RATIO_EV_INSTR 128

/* macro to output register F */
#define FLAGS(r)  \
(r & 0x80 ? 'S' : '.'), \
(r & 0x40 ? 'Z' : '.'), \
(r & 0x20 ? '5' : '.'), \
(r & 0x10 ? 'H' : '.'), \
(r & 0x08 ? '3' : '.'), \
(r & 0x04 ? 'P' : '.'), \
(r & 0x02 ? 'N' : '.'), \
(r & 0x01 ? 'C' : '.')

/* prototypes */
void OpenSdl(void);
void CloseSdl(void);
int ExitWithError(char * message);
void UpdateRTC(void);
void RefreshScreen(SDL_Window * window);
uint8_t KeyboardMatrix(uint8_t Mask);
uint32_t TimerCallBack(uint32_t period, void * param);
char StepZ88(void);
void OneStep(void);
uint16_t DAsm(char *S, word A);
void ViewBlink(void);
void UpdateInt(void);
uint8_t DoEvents(SDL_Event * event);

#endif
